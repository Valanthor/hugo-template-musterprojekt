#!/usr/bin/env bash

while true; do
  read -p "Did you customize htaccess, robots, config, Rake and deploy? (y/n)" yn
  case $yn in
    [Yy]* ) nps --silent clean.stage;
            nps --silent clean.htaccess;
            nps --silent build.stage.hugo;
            nps --silent copy.htaccess;
            break;;

    [Nn]* ) clear;
            echo "---------------------------------------------------"
            echo "Okay, Please do that before going on..."
            echo "Customize .htaccess, humans, config, Rake and deploy"
            echo "and restart stage build."
            echo "---------------------------------------------------"
            exit;;

  * ) echo "Please answer with Yes or No.";;
  esac
done
